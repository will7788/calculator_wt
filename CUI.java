import acm.program.Program;
import acm.graphics.*;      // GCanvas
import javax.swing.*;       // JButton, JTextField, Jwhatever
import java.awt.event.*;    // Event
import java.awt.*;          // Font, colors

public class CUI extends Program
{
    private JTextField infixField;
    private JTextField postfixField;
    private JTextField resultField;
    private JTextField sField;
    private JTextField aField;
    SY s = new SY();
    Postfix p = new Postfix();
    String answ = "";
    String rmbr = "";
    
    public CUI()
    {
        start();
        setSize(390, 435);
        setTitle("Calculator");
    }
    
    public void init()
    {
        GCanvas canvas = new GCanvas();
        add(canvas);
        
        //All top end.
        JLabel infixLabel = new JLabel("Infix:");
        JLabel postfixLabel = new JLabel("Postfix:");
        JLabel slbl = new JLabel("STO:");
        JLabel albl = new JLabel("ANS:");
        JLabel resultLabel = new JLabel("Result:");
        infixField = new JTextField();
        postfixField = new JTextField();
        resultField = new JTextField();
        sField = new JTextField();
        aField = new JTextField();
        JButton goButton = new JButton();
        goButton.setActionCommand("Go");
        JButton clearButton = new JButton();
        clearButton.setActionCommand("Clear");
        
        //Font
        Font bigText = new Font("Arial", Font.BOLD, 24);
        infixLabel.setFont(bigText);
        postfixLabel.setFont(bigText);
        resultLabel.setFont(bigText);
        
        //canvas.setBackground(Color.WHITE);
        
        infixField.setSize(200, 20);
        postfixField.setSize(200, 20);
        resultField.setSize(130, 20);
        aField.setSize(43, 20);
        sField.setSize(43, 20);
        
        
        canvas.add(infixLabel, 50, 10);
        canvas.add(postfixLabel, 50, 40);
        canvas.add(resultLabel, 50, 70);
        canvas.add(albl, 5, 10);
        canvas.add(slbl, 5, 50);
        canvas.add(infixField, 150, 15);
        canvas.add(postfixField, 150, 47);
        canvas.add(resultField, 150, 79);
        canvas.add(aField, 1, 30);
        canvas.add(sField, 1, 70);
        canvas.add(goButton, 300, 75);
        canvas.add(clearButton, 295, 107);
        
        postfixField.setEditable(false);
        resultField.setEditable(false);
        aField.setEditable(false);
        sField.setEditable(false);
        
        ImageIcon goImage = new ImageIcon("GoButton.png");
        goButton.setIcon(goImage);
        goButton.setSize(goImage.getIconWidth(), goImage.getIconHeight());
        //goButton.setBorderPainted(false);
        
        ImageIcon clearImage = new ImageIcon("ClearButton.png");
        clearButton.setIcon(clearImage);
        clearButton.setSize(clearImage.getIconWidth(), clearImage.getIconHeight());
        
        GImage sig = new GImage("Signature.png");
        
        canvas.add(sig, 2, 350);
        
        //Buttons for numbers and whatnot
        JButton one = new JButton("1");
        JButton two = new JButton("2");
        JButton thr = new JButton("3");
        JButton fou = new JButton("4");
        JButton fiv = new JButton("5");
        JButton six = new JButton("6");
        JButton sev = new JButton("7");
        JButton eig = new JButton("8");
        JButton nin = new JButton("9");
        JButton zer = new JButton("0");
        JButton dot = new JButton(".");
        JButton mul = new JButton("*");
        JButton sub = new JButton("-");
        JButton add = new JButton("+");
        JButton div = new JButton("/");
        JButton mod = new JButton("%");
        JButton ope = new JButton("(");
        JButton clo = new JButton(")");
        JButton sin = new JButton("sin");
        JButton cos = new JButton("cos");
        JButton tan = new JButton("tan");
        JButton roo = new JButton("√");
        JButton pow = new JButton("^");
        JButton ans = new JButton("ANS");
        JButton rcl = new JButton("RCL");
        JButton sto = new JButton("STO");
        JButton pi = new JButton("pi");
        
        canvas.add(sev, 65, 120); //(+43, +28)
        canvas.add(eig, 108, 120);
        canvas.add(nin, 151, 120);
        canvas.add(add, 196, 120);
        canvas.add(sin, 240, 120);
        canvas.add(fou, 65, 148);
        canvas.add(fiv, 108, 148);
        canvas.add(six, 151, 148);
        canvas.add(sub, 196, 148);
        canvas.add(cos, 240, 148);
        canvas.add(one, 65, 176);
        canvas.add(two, 108, 176);
        canvas.add(thr, 151, 176);
        canvas.add(mul, 196, 176);
        canvas.add(tan, 240, 176);
        canvas.add(dot, 65, 204);
        canvas.add(zer, 108, 204);
        canvas.add(mod, 151, 204);
        canvas.add(div, 196, 204);
        canvas.add(pow, 240, 204);
        canvas.add(pi, 106, 232);
        canvas.add(ope, 65, 232);
        canvas.add(clo, 152, 232);
        canvas.add(roo, 240, 232);
        
        canvas.add(ans, 300, 135);
        canvas.add(rcl, 300, 163);
        canvas.add(sto, 300, 191);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();
        if (whichButton == "Go" || whichButton == "Clear" || whichButton == "ANS" || whichButton == "RCL" || whichButton == "STO")
        {
            switch (whichButton)
            {
                case "Go":
                postfixField.setText("" + s.convert(infixField.getText()));
                resultField.setText("" + p.eval(s.convert(infixField.getText())));
                answ = resultField.getText();
                aField.setText(answ);
                break;
            
                case "Clear":
                infixField.setText("");
                postfixField.setText("");
                resultField.setText("");
                break;
                
                case "ANS":
                infixField.setText(infixField.getText() + answ + " ");
                break;
                
                case "RCL":
                infixField.setText(infixField.getText() + rmbr + " ");
                break;
                
                case "STO":
                rmbr = resultField.getText();
                sField.setText(rmbr);
                break;
            }
        }
        else
        {
            infixField.setText(infixField.getText() + whichButton + " ");
        }
    }
}