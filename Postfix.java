import java.util.ArrayList;
public class Postfix
{
    public double eval(String expr)
    {
        String[] tokens = expr.split(" ");
        return eval(tokens);
    }
    
    public double eval(ArrayList<String> tokens)
    {
        return eval(tokens.toArray(new String[tokens.size()]));
    }
    public double eval(String[] tokens)
    {
        Stack<Double> s = new Stack<Double>();
        
        for (int i = 0; i < tokens.length; i++)
        {
            String token = tokens[i];
            switch(token)
            {
                case "+":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b + a);
                    break;
                    }
                case "*":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b * a);
                    break;
                    }
                case "-":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b - a);
                    break;
                    }
                case "/":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b / a);
                    break;
                    }
                case "%":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b % a);
                    break;
                    }
                case "^":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(Math.pow(b, a));
                    break;
                    }
                case "√":
                    {
                    s.push(Math.sqrt(s.pop()));
                    break;
                    }
                case "pi":
                    {
                    s.push(Math.PI);
                    break;
                    }
                case "cos":
                    {
                    s.push(Math.cos(s.pop()));
                    break;
                    }
                case "sin":
                    {
                    s.push(Math.sin(s.pop()));
                    break;
                    }
                case "tan":
                    {
                    s.push(Math.tan(s.pop()));
                    break;
                    }
                default:
                    {
                        try
                        {
                            s.push(Double.parseDouble(token));
                            break;
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                            throw new InvalidTokenException();
                        }
                    }
            
            
                }
        }
        return s.peek();
    }
    
}