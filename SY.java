import java.util.ArrayList;

public class SY
{
    public ArrayList<String> convert(String expr)
    {
        String[] tokens = expr.split(" +");
        ArrayList<String> result = new ArrayList<String>();
        Stack<Stack<String>> stack = new Stack<Stack<String>>();
        stack.push(new Stack<String>());
        for (String token : tokens)
        {   
            if (token.equals("pi"))
            {
                result.add(token);
            }
            if (token.equals("("))
            {
                stack.push(new Stack<String>());
            }
            if (token.equals(")"))
            {
                while (!stack.peek().isEmpty())
                {
                    result.add(stack.peek().pop());
                }
                stack.pop();
            }
            if (prec(token) != -1)  // It's an operator!
            {
                if (stack.peek().isEmpty())
                {
                    stack.peek().push(token);
                }
                else if (prec(stack.peek().peek()) < prec(token))
                {
                    stack.peek().push(token);
                }
                else
                {
                    while (!stack.peek().isEmpty() && prec(stack.peek().peek()) >= prec(token))
                    {
                        result.add(stack.peek().pop());
                    }
                    stack.peek().push(token);
                }
            }
            else  // Must be a number
            {
                try     {double a = Double.parseDouble(token);}
                catch   (java.lang.NumberFormatException e)
                        {continue;}
                result.add(token);
            }
        }
        
        while (!stack.peek().isEmpty())
            {
            result.add(stack.peek().pop());
            }
        return result;
    }
    private int prec(String op)
    {
        switch(op)
        {
            case "+":
            case "-":
            return 1;
            
            case "*":
            case "/":
            case "%":
            return 2;
            
            case "^":
            case "√":
            return 3;
            
            case "cos":
            case "sin":
            case "tan":
            return 4;
            
            default:
            return -1;
        }
    }
}